
let students = [];

function addStudent(studentName){

	students.push(studentName);
	console.log(`${studentName} added to the student's list.`);
}

function countStudents() {
	if (students.length == 0) {
		console.log("Please add student name to students list.")
	} else {
		console.log('There are total of ' + students.length + " students enrolled.");
	}
}


function printStudents(){
	
	students.sort().forEach(function(student) {
		console.log(student);
	})
}


function findStudent(search) {
	let find = students.filter(function(student){
		return student.toLowerCase().includes(search.toLowerCase());
	})

	if (find.length == 1) {
		console.log(`${find.toString()} is an Enrollee.`)
	} else if (find.length > 1) {
		console.log(`${find.join(', ')} are enrollees.`)
	} else {
		console.log(`No student found with keyword or name ${search}.`)
	}
}

function addSection(section){
	let newSection = students.map(function (name){
		return name + " - Section " + section;
	})
	console.log(newSection);
}

function removeStudent(name){
	for (var i = 0; i < students.length; i++) {
		if (students[i] == name){
			console.log(`${students[i]} is was removed from the student's list.`);
			students.splice(i, 1);
		} else {
			console.log(`${name} is not in the list. Please enter the correct student name.`)
		}
	}
}